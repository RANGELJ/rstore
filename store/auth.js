
export const state = () => ({
    logedIn: false,
    userFirstName: "",
    userLastName: "",
});

export const mutations = {
    SET_IS_LOGED_IN(state, isLogedIn) {
        state.logedIn = isLogedIn;
    },
};

export const actions = {
    LOGIN: async ({commit}, {username, password}) => {
        commit("SET_IS_LOGED_IN", true);
        return;
    },
};
