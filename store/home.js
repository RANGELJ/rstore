
export const state = () => ({
    sections: [],
});

export const mutations = {
    SET_SECTIONS(state, newSections) {
        state.sections = newSections;
    },
};

export const actions = {
    async LOAD_SECTIONS({commit}) {
        // Here shoud be the query to get the sections
        commit("SET_SECTIONS", [
            {
                title: "Almacen",
                to: "/almacen",
                description: "Un modulo",
                icon: "business",
            },
        ]);
        return;
    },
};
