
export const state = () => ({
    productsOnList: [],
});

export const mutations = {
    SET_PRODUCTS_ON_LIST(state, newProducts) {
        state.productsOnList = newProducts;
    },
};

export const actions = {
    async LOAD_PRODUCTS_ON_LIST({commit}) {
        commit("SET_PRODUCTS_ON_LIST", [
            {
                id: 1,
                name: "Producto 1",
                description: "Un producto",
                unitType: "unit"},
            {
                id: 2,
                name: "Producto 2",
                description: "Otro producto",
                unitType: "weight",
            },
        ]);
        return;
    },
};
