
export const state = () => ({
    modules: [],
});

export const mutations = {
    SET_MODULES(state, newModules) {
        state.modules = newModules;
    },
};

export const actions = {
    async LOAD_MODULES({commit}) {
        commit("SET_MODULES", [
            {
                id: 1,
                url: "/almacen/productos",
                title: "Productos",
                icon: "shopping_basket",
            },
            {
                id: 2,
                url: "/almacen/existencias",
                title: "Existencias",
                icon: "ballot",
            },
            {
                id: 3,
                url: "/almacen/traspasos",
                title: "Traspasos",
                icon: "redo",
            },
        ]);
        return;
    },
};
