import Vue from "vue";
import {
    Vuetify,
    VApp,
    VCard,
    VNavigationDrawer,
    VFooter,
    VList,
    VBtn,
    VIcon,
    VGrid,
    VToolbar,
    VTextField,
    VDataTable,
    VAutocomplete,
} from "vuetify";
import es from "../local_modules/vuetify_locales/es";

Vue.use(Vuetify, {
    lang: {
        locales: {es},
        current: "es",
    },
    components: {
        VApp,
        VCard,
        VNavigationDrawer,
        VFooter,
        VList,
        VBtn,
        VIcon,
        VGrid,
        VToolbar,
        VTextField,
        VDataTable,
        VAutocomplete,
    },
    theme: {
        primary: "#00838F",
        secondary: "#E0F2F1",
        accent: "#82B1FF",
        error: "#FF5252",
        info: "#2196F3",
        success: "#4CAF50",
        warning: "#FFC107",
    },
});
