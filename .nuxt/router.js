import Vue from 'vue'
import Router from 'vue-router'

const _408a1a46 = () => import('../pages/almacen.vue' /* webpackChunkName: "pages/almacen" */).then(m => m.default || m)
const _1dcf5449 = () => import('../pages/almacen/index.vue' /* webpackChunkName: "pages/almacen/index" */).then(m => m.default || m)
const _acd912ec = () => import('../pages/almacen/productos.vue' /* webpackChunkName: "pages/almacen/productos" */).then(m => m.default || m)
const _b2e924e6 = () => import('../pages/almacen/productos/index.vue' /* webpackChunkName: "pages/almacen/productos/index" */).then(m => m.default || m)
const _f9c6775c = () => import('../pages/almacen/productos/nuevoproducto.vue' /* webpackChunkName: "pages/almacen/productos/nuevoproducto" */).then(m => m.default || m)
const _7302da56 = () => import('../pages/home.vue' /* webpackChunkName: "pages/home" */).then(m => m.default || m)
const _4195b3eb = () => import('../pages/index.vue' /* webpackChunkName: "pages/index" */).then(m => m.default || m)

Vue.use(Router)


if (process.client) {
  window.history.scrollRestoration = 'manual'
}
const scrollBehavior = function (to, from, savedPosition) {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false

  // if no children detected
  if (to.matched.length < 2) {
    // scroll to the top of the page
    position = { x: 0, y: 0 }
  } else if (to.matched.some((r) => r.components.default.options.scrollToTop)) {
    // if one of the children has scrollToTop option set to true
    position = { x: 0, y: 0 }
  }

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  return new Promise(resolve => {
    // wait for the out transition to complete (if necessary)
    window.$nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash) {
        let hash = to.hash
        // CSS.escape() is not supported with IE and Edge.
        if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
          hash = '#' + window.CSS.escape(hash.substr(1))
        }
        try {
          if (document.querySelector(hash)) {
            // scroll to anchor by returning the selector
            position = { selector: hash }
          }
        } catch (e) {
          console.warn('Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).')
        }
      }
      resolve(position)
    })
  })
}


export function createRouter () {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,
    routes: [
		{
			path: "/almacen",
			component: _408a1a46,
			children: [
				{
					path: "",
					component: _1dcf5449,
					name: "almacen"
				},
				{
					path: "productos",
					component: _acd912ec,
					children: [
						{
							path: "",
							component: _b2e924e6,
							name: "almacen-productos"
						},
						{
							path: "nuevoproducto",
							component: _f9c6775c,
							name: "almacen-productos-nuevoproducto"
						}
					]
				}
			]
		},
		{
			path: "/home",
			component: _7302da56,
			name: "home"
		},
		{
			path: "/",
			component: _4195b3eb,
			name: "index"
		}
    ],
    
    
    fallback: false
  })
}
