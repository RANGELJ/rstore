module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
  },
  env: {
    browser: true,
    node: true
  },
  extends: ["eslint:recommended", "google", 'plugin:vue/recommended'],
  // required to lint *.vue files
  plugins: [
    "vue",
  ],
  // add your custom rules here
  rules: {
    quotes: ["error", "double"],
    "indent": ["error", 4],
    "vue/html-indent": ["error",4],
    "vue/max-attributes-per-line": ["error", {
      "singleline": 5,
      "multiline": {
        "max": 1,
        "allowFirstLine": false
      }
    }],
  },
  globals: {}
}
