export default {
    dataIterator: {
        rowsPerPageText: "Elementos por pagina:",
        rowsPerPageAll: "Todos",
        pageText: "{0}-{1} de {2}",
        noResultsText: "No se encontraron coincidencias",
        nextPage: "Siguiente pagina",
        prevPage: "Pagina previa",
    },
    dataTable: {
        rowsPerPageText: "Filas por pagina:",
    },
    noDataText: "Sin informacion disponible",
};
